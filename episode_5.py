#!/bin/python
"""
episode_5.py Is for solving episode 5 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 5"
    args = {
        '<color>': 'blue',
        '--cipher_key': 'thunderstorms',
        '<number_sequence>': [
            "21", "22", "12", "01", "17", "13", "05",
            "23", "13", "19", "22", "26", "06", "02",
            "21", "25", "07", "02", "08", "26", "23",
            "23", "02", "26", "26", "23", "13", "12",
            "25", "01", "09", "19", "09", "12", "18",
            "20", "26", "08", "23"
        ]
    }
    main(args)
