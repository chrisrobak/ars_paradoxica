#!/bin/python
"""
episode_6.py Is for solving episode 6 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 6"
    args = {
        '<color>': 'red',
        '--cipher_key': 'foggy',
        '<number_sequence>': [
            "20", "18", "01", "01", "24", "04", "07",
            "09", "22", "10", "23", "25", "17", "03",
            "14", "09", "08"
        ]
    }
    main(args)
