"""
Script to decode the digits at the end of the ars paradoxica podcast.

Usage:
    ars_paradoxica.py <color> <number_sequence>... [options]

Options:
    -h --help            Shows this screen
    --cipher_key=<cipher_key>
"""
import sys
sys.path.append('/home/lbc/zenforweb/toolkit')
from two_digits_to_alpha import TwoDigitToAlpha
from atbash import AtBash
from vigenere import Vigenere
import rot_decode
from docopt import docopt


def handle_blue_code(cipher_key, number_sequence):
    if not cipher_key:
        raise ValueError('Blue Codes require a cipher key')
    tda = TwoDigitToAlpha(number_sequence)
    tda.translate()
    vg = Vigenere(ciphertext=tda.translation, cipherkey=cipher_key)
    vg.decode()
    print "    %s" % vg.clear_text


def handle_red_code(cipher_key, number_sequence):
    if not cipher_key:
        raise ValueError('Red Codes require a cipher key')
    tda = TwoDigitToAlpha(number_sequence)
    tda.translate()
    atb = AtBash(tda.translation)
    atb.translate()
    for translation in rot_decode.run(atb.result, 26):
        vg = Vigenere(ciphertext=translation, cipherkey=cipher_key)
        vg.decode()
        print "    %s" % vg.clear_text


def main(args):
    color = str(args['<color>']).lower()
    number_sequence = [int(x) for x in args['<number_sequence>']]
    cipher_key = args.get('--cipher_key')
    if color == 'blue':
        handle_blue_code(cipher_key, number_sequence)
    elif color == 'red':
        handle_red_code(cipher_key, number_sequence)
    else:
        raise ValueError('Error: %s not a valid code color' % color)


if __name__ == '__main__':
    args = docopt(__doc__)
    main(args)
