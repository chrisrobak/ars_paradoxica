#!/bin/python
"""
episode_3.py Is for solving episode 3 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 3"
    # part 1
    args_1 = {
        '<color>': 'blue',
        '--cipher_key': 'windy',
        '<number_sequence>': [
            "03", "23", "02", "07", "07", "10", "02",
            "18", "17", "18", "05", "23", "01", "22",
            "11", "01", "09", "01", "22", "15", "17",
            "09", "07"
        ]
    }

    # part 2
    args_2 = {
        '<color>': 'red',
        '--cipher_key': 'windy',
        '<number_sequence>': [
            "08", "11", "09", "10", "04", "16", "24",
            "17", "19", "02", "15", "04", "26", "15",
            "03", "26", "03", "06", "25", "10", "22",
            "16", "21"
        ]
    }
    print "  Part 1"
    main(args_1)
    print "  Part 2"
    main(args_2)
