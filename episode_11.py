#!/bin/python
"""
episode_11.py Is for solving episode 11 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 11"
    args = {
        '<color>': 'red',
        '--cipher_key': 'foggy',
        '<number_sequence>': [
            "20", "08", "24", "20", "11", "17", "08",
            "02", "18", "02", "10", "12", "01", "12",
            "14", "08"
        ]
    }
    main(args)
