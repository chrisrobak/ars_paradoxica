#!/bin/python
"""
episode_9.py Is for solving episode 9 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 9"
    args = {
        '<color>': 'blue',
        '--cipher_key': 'showers',
        '<number_sequence>': [
            "12", "16", "01", "01", "01", "26", "04",
            "04", "18", "23", "08", "16", "12", "11",
            "19", "19", "26"
        ]
    }
    main(args)
