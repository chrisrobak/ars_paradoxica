#!/bin/python
"""
episode_2.py Is for solving episode 2 of ars_paradoxica
"""
import ars_paradoxica

if __name__ == '__main__':
    print "Episode 2"
    args = {
        '<color>': 'blue',
        '--cipher_key': 'sunny',
        '<number_sequence>': [
            "08", "12", "02", "20", "16", "23", "13",
            "06", "05", "03", "09", "15", "22", "05",
            "03", "11", "13", "14", "16", "16", "01",
            "26", "22", "16", "03", "19", "08", "07",
            "21", "13", "06", "19"            
        ]
    }
    ars_paradoxica.main(args)
