#!/bin/python
"""
episode_4.py Is for solving episode 4 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 4"
    # part 1
    args = {
        '<color>': 'red',
        '--cipher_key': 'cloudy',
        '<number_sequence>': [
            "17", "11", "04", "14", "18", "11", "10",
            "03", "20", "18", "11", "24", "02", "08",
            "08", "19", "19", "17", "06", "11"
        ]
    }
    main(args)
