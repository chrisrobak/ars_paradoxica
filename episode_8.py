#!/bin/python
"""
episode_8.py Is for solving episode 8 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 8"
    args = {
        '<color>': 'red',
        '--cipher_key': 'showers',
        '<number_sequence>': [
            "17", "11", "23", "15", "11", "05", "16",
            "26", "06", "19", "23", "18", "07", "20",
            "16", "07", "04", "02", "07", "21", "21",
            "05"
        ]
    }
    main(args)
