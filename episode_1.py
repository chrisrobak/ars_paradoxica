#!/bin/python
"""
episode_1.py Is for solving episode 1 of ars_paradoxica
"""
import ars_paradoxica

if __name__ == '__main__':
    print "Episode 1"
    args = {
        '<color>': 'blue',
        '--cipher_key': 'cloudy',
        '<number_sequence>': [
            "10", "16", "26", "06", "18", "04",
            "20", "26", "01", "14", "11", "03",
            "08", "06", "08", "15", "21", "03"
        ]
    }
    ars_paradoxica.main(args)
