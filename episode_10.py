#!/bin/python
"""
episode_10.py Is for solving episode 10 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 10"
    # part 1 & part 2 are the same
    args = {
        '<color>': 'red',
        '--cipher_key': 'clear',
        '<number_sequence>': [
            "06", "15", "11", "15", "11", "22", "15",
            "09", "02", "21", "04", "08", "18", "26",
            "18", "12", "11"
        ]
    }
    main(args)
