#!/bin/python
"""
episode_7.py Is for solving episode 7 of ars_paradoxica
"""
from ars_paradoxica import main

if __name__ == '__main__':
    print "Episode 7"
    args = {
        '<color>': 'blue',
        '--cipher_key': 'showers',
        '<number_sequence>': [
            "21", "25", "15", "25", "15", "10", "01",
            "06", "01", "22", "01", "23", "12", "10",
            "24", "08", "17", "01"
        ]
    }
    main(args)
